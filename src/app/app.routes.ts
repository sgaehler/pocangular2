import {Routes} from '@angular/router';
import {Home} from './home/home';
import {Newspage} from './newspage/newspage';
import {Document} from './document/document';

export const rootRouterConfig: Routes = [
  {path: '', component: Home},
  {path: '/newspage', component: Newspage},
  {path: ':type/:id', component: Document}
];
